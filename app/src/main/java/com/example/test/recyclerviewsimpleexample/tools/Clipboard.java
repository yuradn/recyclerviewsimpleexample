package com.example.test.recyclerviewsimpleexample.tools;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Stas on 27.08.2015.
 */
public class Clipboard {
    private final static String TAG = "Clipdata";

    public static boolean intoClipboard(@NonNull Context ctx, @NonNull Iterable<String> iter){
        ClipboardManager clipboardManager = (ClipboardManager) ctx.getSystemService(Context.CLIPBOARD_SERVICE);
        String moore="";

        for (String s : iter) {
            Log.e(TAG, s);
            moore += s + "\n";
        }

        Log.d(TAG, "All clipdata: " + moore);

        ClipData clipData = ClipData.newPlainText("label", moore);

        if(clipData != null){
            if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                clipboardManager.setPrimaryClip(clipData);
            }
            return true;
        } else {
            return false;
        }
    }

    public static boolean toClipboard(@NonNull Context ctx, @NonNull String str){
        ClipboardManager clipboardManager = (ClipboardManager) ctx.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText("label", str);

        if(clipData != null){
            if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                clipboardManager.setPrimaryClip(clipData);
            }
            return true;
        } else {
            return false;
        }
    }

    @NonNull
    public static List<CharSequence> ontoClipboard(@NonNull Context ctx){
        List<CharSequence> result = new LinkedList<>();
        ClipboardManager clipboardManager = (ClipboardManager) ctx.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = clipboardManager.getPrimaryClip();
        for(int i = 0; i < clipData.getItemCount(); ++i) {
            Log.d("ontoClipboard", clipData.getItemAt(i).toString());
            result.add(clipData.getItemAt(i).getText());
        }
        return result;
    }
}