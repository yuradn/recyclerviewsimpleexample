package com.example.test.recyclerviewsimpleexample.tools.select;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ViewGroup;

/**
 * Created by test on 11/2/15.
 */
public class SelectionController {

    private ViewGroup selectableViewGroup;
    private GestureDetector gestureDetector;
    private boolean selectInProcess = false;

    public SelectionController (ViewGroup selectableViewGroup) {
        this.selectableViewGroup = selectableViewGroup;
    }

    private void initGesture() {
        gestureDetector = new GestureDetector(selectableViewGroup.getContext(), new GestureDetector.SimpleOnGestureListener() {
            @Override
            public void onLongPress(MotionEvent event) {
                if (!selectInProcess) {
                    //startSelection(event);
                }
            }
        });
    }

}
