package com.example.test.recyclerviewsimpleexample;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.example.test.recyclerviewsimpleexample.tools.KeyboardUtils;
import com.example.test.recyclerviewsimpleexample.tools.LoremIpsum;
import com.example.test.recyclerviewsimpleexample.tools.common.BaseActivity;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.Bind;

public class MainActivity extends BaseActivity {
    private final static String TAG = MainActivity.class.getSimpleName();

    @Bind(R.id.edtMessage)
    EditText edtMessage;

    @Bind(R.id.recyclerView)
    protected RecyclerView recyclerView;

    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    ArrayList<String> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(edtMessage.getText())) {
                    //Toast.makeText(MainActivity.this, "Cool!", Toast.LENGTH_LONG).show();
                    Snackbar.make(view, "Cool!", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    data.add(edtMessage.getText().toString());
                    mAdapter.notifyItemInserted(data.size() - 1);
                    edtMessage.setText("");
                    KeyboardUtils.hideKeyboard(MainActivity.this);
                } else {
                    //Toast.makeText(MainActivity.this, "Nothing!", Toast.LENGTH_LONG).show();
                    Snackbar.make(view, "Nothing!", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }

            }
        });

        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        data = new ArrayList<>();
        Collections.addAll(data, LoremIpsum.mData);
        mAdapter = new MyAdapter(MainActivity.this, data);
        recyclerView.setAdapter(mAdapter);

    }

    @Override
    public int setLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cab_chat, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        ContextMenu.ContextMenuInfo info = item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.item_copy:
                copyNote(info);
                return true;
            /*case R.id.edit:
                editNote(info.id);
                return true;
            case R.id.delete:
                deleteNote(info.id);
                return true;*/
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void copyNote(ContextMenu.ContextMenuInfo id) {
        Log.d(TAG, "Id: " + id.toString());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(com.example.test.recyclerviewsimpleexample.R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == com.example.test.recyclerviewsimpleexample.R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
