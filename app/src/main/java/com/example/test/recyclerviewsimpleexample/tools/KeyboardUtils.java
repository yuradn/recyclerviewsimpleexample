package com.example.test.recyclerviewsimpleexample.tools;

import android.app.Activity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by test on 11/4/15.
 */
public class KeyboardUtils {

    public static void hideKeyboard(Activity act){
        View view = act.getCurrentFocus();
        InputMethodManager imm = (InputMethodManager) act.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (view != null && imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

}
