package com.example.test.recyclerviewsimpleexample.tools.select;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;

/**
 * Created by test on 11/2/15.
 */
public class SelectableLayoutManager extends LinearLayoutManager {

    private SelectionController sh;

    public SelectableLayoutManager(Context context) {
        super(context);
    }

    public SelectableLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    public SelectableLayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setSelectionController(SelectionController selectionController) {
        sh = selectionController;
    }
}
