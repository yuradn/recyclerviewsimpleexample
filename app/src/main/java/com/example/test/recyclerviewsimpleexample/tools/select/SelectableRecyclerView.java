package com.example.test.recyclerviewsimpleexample.tools.select;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by test on 11/2/15.
 */
public class SelectableRecyclerView extends RecyclerView {

    private SelectionController sh;

    public SelectableRecyclerView(Context context) {
        super(context);
    }

    public SelectableRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SelectableRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        sh = new SelectionController(this);
    }


    @Override
    public void setLayoutManager(LayoutManager layout) {
        super.setLayoutManager(layout);
        if (layout instanceof SelectableLayoutManager) {
            ((SelectableLayoutManager) layout).setSelectionController(sh);
        }
    }
}
