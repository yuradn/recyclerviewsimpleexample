package com.example.test.recyclerviewsimpleexample;

import android.support.v4.content.ContextCompat;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.test.recyclerviewsimpleexample.tools.Clipboard;
import com.example.test.recyclerviewsimpleexample.tools.common.BaseActivity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by test on 10/19/15.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private final static String TAG = "RecyclerViewAdapter";
    private ArrayList<String> mDataset;
    private BaseActivity activity;
    private HashMap<Integer, Boolean> selected;
    private OnItemClickOperation onItemClickOperation;
    private int colorSelectBackground;
    private int colorTransparent;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            View.OnLongClickListener {
        // each data item is just a string in this case
        public OnItemClickCallback onItemClickCallback;
        public TextView tvContent;
        public Button btnContent;
        public ImageView imgView;


        public ViewHolder(View v) {
            super(v);
            tvContent = (TextView) v.findViewById(R.id.tvContent);
            btnContent = (Button) v.findViewById(R.id.btnContent);
            btnContent.setOnClickListener(this);
            btnContent.setOnLongClickListener(this);
            imgView = (ImageView) v.findViewById(R.id.imgView);
            imgView.setOnClickListener(this);
        }

        public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
            this.onItemClickCallback = onItemClickCallback;
        }

        @Override
        public void onClick(View v) {

            if (onItemClickCallback != null) {
                onItemClickCallback.onItemClicked(v, getAdapterPosition());
            }

        }

        @Override
        public boolean onLongClick(View v) {

            if (onItemClickCallback != null) {
                onItemClickCallback.onItemLongClicked(v, getAdapterPosition());
            }
            return false;
        }
    }

    public static class TimeViewHolder extends RecyclerView.ViewHolder {

        public TimeViewHolder(View itemView) {
            super(itemView);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(BaseActivity activity, ArrayList<String> myDataset) {
        mDataset = myDataset;
        this.activity = activity;
        selected = new HashMap<>();
        colorSelectBackground = ContextCompat.getColor(activity, R.color.gray);
        colorTransparent = ContextCompat.getColor(activity, android.R.color.transparent);
        onItemClickOperation = new OnItemClickOperation(activity, selected);
    }



    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recyclerview, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        vh.setOnItemClickCallback(onItemClickOperation);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.tvContent.setText(mDataset.get(position));
        //Log.d(TAG, "Holder class: " + holder.getClass().getCanonicalName());
        //Log.d(TAG, "Parent class: " + holder.tvContent.getParent().getClass().getCanonicalName());
        RelativeLayout parentRelative = (RelativeLayout) holder.tvContent.getParent();
        if (selected.get(position) != null) {
            parentRelative
                    .setBackgroundColor(colorSelectBackground);
        } else {
            parentRelative
                    .setBackgroundColor(colorTransparent);
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    private void deleteSelectedItems() {
        ArrayList<Integer> listSelectedId = new ArrayList<>();
        for (Integer id : selected.keySet()) {
            listSelectedId.add(id);
        }
        for (Integer id : listSelectedId) {
            selected.remove(id);
            notifyItemChanged(id);
        }
    }

    private void shareSelectedItems() {
        String str = "";
        ArrayList<Integer> listSelectedId = new ArrayList<>();
        for (Integer id : selected.keySet()) {
            str += mDataset.get(id) + "\n";
            listSelectedId.add(id);
        }
        for (Integer id : listSelectedId) {
            selected.remove(id);
            notifyItemChanged(id);
        }
        if (!TextUtils.isEmpty(str)) {
            if (str.length()>120) {
                str = str.substring(0,117) + "...";
            }
            Toast.makeText(activity, str, Toast.LENGTH_SHORT).show();
        }
        Clipboard.toClipboard(activity, str);
    }

    public class OnItemClickOperation implements OnItemClickCallback {

        private ActionMode actionMode;
        private BaseActivity activity;
        private HashMap<Integer, Boolean> selected;

        public OnItemClickOperation(BaseActivity activity, HashMap<Integer, Boolean> selected) {
            this.activity = activity;
            this.selected = selected;
        }

        @Override
        public void onItemClicked(View view, int position) {
            //Log.d(TAG, "OnClick");
            //Log.d(TAG, "GetAdapterPosition: " + position + " view: " + view.getClass().getCanonicalName());
            if (actionMode != null) {
                if (selected.get(position) != null) {
                    selected.remove(position);
                } else {
                    selected.put(position, true);
                }
                setSizeClipboard(selected.size());
                //notifyDataSetChanged();
                notifyItemChanged(position);
            }
        }

        @Override
        public void onItemLongClicked(View view, int position) {
            //Log.d(TAG, "OnLongClick");
            //Log.d(TAG, "GetAdapterPosition: " + position + " view: " + view.getClass().getCanonicalName());
            if (actionMode == null) {
                actionMode = activity.startSupportActionMode(mActionModeCallback);
                selected.clear();
                selected.put(position, true);
                setSizeClipboard(selected.size());
            } else {
                selected.remove(position);
                if (selected.size() == 0) {
                    actionMode.finish();
                }
            }
            notifyItemChanged(position);

        }

        private void setSizeClipboard(int size) {
            actionMode.setTitle(activity.getString(R.string.abc_chat_copy) + " : " + size);
        }

        private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

            // Called when the action mode is created; startActionMode() was called
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                // Inflate a menu resource providing activity menu items
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.menu_cab_chat, menu);
                return true;
            }

            // Called each time the action mode is shown. Always called after onCreateActionMode, but
            // may be called multiple times if the mode is invalidated.
            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                //Log.d(TAG, "On prepare.");
                hideToolbar();
                return false; // Return false if nothing is done
            }

            // Called when the user selects a contextual menu item
            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.item_copy:
                        //Log.d(TAG, "Item copy");
                        shareSelectedItems();
                        mode.finish();
                        showToolbar();
                        //notifyDataSetChanged();
                        actionMode = null;
                        return true;
                    default:
                        return false;
                }
            }

            // Called when the user exits the action mode
            @Override
            public void onDestroyActionMode(ActionMode mode) {
                //Log.d(TAG, "On destroy.");
                deleteSelectedItems();
                mode.finish(); // Action picked, so close the CAB
                showToolbar();
                //notifyDataSetChanged();
                actionMode = null;
            }

            private void showToolbar() {
                //activity.getToolbar().setVisibility(View.VISIBLE);
                activity.getSupportActionBar().show();
            }

            private void hideToolbar() {
                activity.getSupportActionBar().hide();
                //activity.getToolbar().setVisibility(View.GONE);
            }


        };

    }

    public interface OnItemClickCallback {
        void onItemClicked(View view, int position);

        void onItemLongClicked(View view, int position);
    }

    public interface OnItemSelection {
        void onItemSelected(int position);

        void onClearAllSelect();
    }

}